﻿using System;

// Define an interface for the fruit
public interface IFruit
{
    void Eat();
}

// Concrete implementation of Apple
public class Apple : IFruit
{
    public void Eat()
    {
        Console.WriteLine("Eating an apple.");
    }
}

// Concrete implementation of Banana
public class Banana : IFruit
{
    public void Eat()
    {
        Console.WriteLine("Peeling and eating a banana.");
    }
}

// Fruit Factory
public class FruitFactory
{
    // Factory method to create fruits based on a provided type
    public static IFruit CreateFruit(string fruitType)
    {
        switch (fruitType.ToLower())
        {
            case "apple":
                return new Apple();
            case "banana":
                return new Banana();
            default:
                throw new ArgumentException("Invalid fruit type.");
        }
    }
}

class Program
{
    static void Main()
    {
        // Use the factory to create fruits
        IFruit apple = FruitFactory.CreateFruit("Apple");
        IFruit banana = FruitFactory.CreateFruit("Banana");

        apple.Eat();
        banana.Eat();
    }
}
